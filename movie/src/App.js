import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import LoginPage from "./Components/LoginPage/index";
import HomePage from "./Components/HomePage/index";
import MoviesPage from "./Components/MoviesPage/index";

function App() {
  return (
    <div>
      <Switch>
        <Route exact path="/" render={() => <Redirect to="/login" />} />
        <Route path="/login" component={LoginPage} />
        <Route path="/homepage" component={HomePage} />
        <Route path="/movie/:id" component={MoviesPage} />
      </Switch>
    </div>
  );
}

export default App;
