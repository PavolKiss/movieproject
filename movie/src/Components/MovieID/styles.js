import styled from "@emotion/styled";
import { Link } from "react-router-dom";
import bgAvengers from "../../Assets/bgAvengers.gif";

export const Movie = styled(Link)`
  font-family: "Raleway", sans-serif;
  background-color: white;
  text-decoration: none;
  color: #4d4d4d;
  border: 5px solid #e6e6e6;
  font-size: 36px;
  font-weight: bold;
  padding: 250px 150px;
  margin-right: 20px;
  transition: 0.5s;
  -webkit-transition: 0.5s;
  &:hover {
    background-image: linear-gradient(
        rgba(255, 255, 255, 0.5),
        rgba(255, 255, 255, 0.5)
      ),
      url(${bgAvengers});
    color: black;
    border: 5px solid #404040;
  }
`;
