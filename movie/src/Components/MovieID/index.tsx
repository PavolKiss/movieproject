import React, { FC } from "react";
import { Movie } from "./styles";

interface IMovieComponent {
  id: number;
  name: string;
  to: string;
}

export const MovieID: FC<IMovieComponent> = ({ id, name, to }) => {
  return (
    <Movie id={id} to={to}>
      {name}
    </Movie>
  );
};
