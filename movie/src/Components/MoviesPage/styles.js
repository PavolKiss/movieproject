import styled from "@emotion/styled";
import { Link } from "react-router-dom";

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  font-family: "Raleway", sans-serif;
  text-align: center;
  margin-top: 15%;
`;

export const MovieTitle = styled.div`
  font-weight: bold;
  text-decoration: underline;
  font-size: 36px;
`;

export const Description = styled.div`
  font-family: "Literata", serif;
  margin-top: 20px;
  font-size: 20px;
`;

export const MovieWrapper = styled.div`
  width: 50%;
  background: white;
  border: 5px solid #e6e6e6;
`;

export const ErrorMsg = styled.div`
  margin-top: 50px;
  font-size: 36px;
  color: #ce2c52;
`;

export const BackLink = styled(Link)`
  text-decoration: none;
  color: black;
  text-transform: uppercase;
  font-size: 16px;
`;
