import React from "react";
import axios from "axios";
import {
  MovieTitle,
  Description,
  Wrapper,
  MovieWrapper,
  ErrorMsg,
  BackLink
} from "./styles";

export default class MoviesPage extends React.Component<{ match: any }> {
  state = {
    title: "",
    description: "",
    errorMsg: ""
  };

  componentDidMount() {
    this.getMovie();
  }

  getMovie = async () => {
    const token = window.sessionStorage.getItem("token");
    const id = this.props.match.params.id;
    try {
      const response = await axios({
        method: "get",
        url: `http://wdassignment.devfl.com/api/movie?id=${id}`,
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      this.setState({
        title: response.data.data.name,
        description: response.data.data.description
      });
    } catch (error) {
      error.response.status &&
        this.setState({
          errorMsg: "No data for this movie. Please choose another."
        });
    }
  };
  render() {
    const { title, description, errorMsg } = this.state;
    return (
      <Wrapper>
        <MovieWrapper>
          {errorMsg ? (
            <ErrorMsg>{errorMsg}</ErrorMsg>
          ) : (
            <div>
              <MovieTitle>{title}</MovieTitle>
              <Description>{description}</Description>
            </div>
          )}
          <div style={{marginTop: 25}}>
            <BackLink to="/homepage">Back</BackLink>
          </div>
        </MovieWrapper>
      </Wrapper>
    );
  }
}
