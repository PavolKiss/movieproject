import styled from "@emotion/styled";

export const Input = styled.input``;

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 15%;
`;
