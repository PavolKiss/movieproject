import React from "react";
import axios from "axios";
import { MovieID } from "./../MovieID/index";
import { Wrapper } from "./styles";
import { TitleWrapper } from "../LoginPage/styles";

export default class HomePage extends React.Component {
  state = {
    movies: [],
    errorMsg: ""
  };

  componentDidMount() {
    this.getMovieIDs();
  }

  getMovieIDs = async () => {
    const token = window.sessionStorage.getItem("token");
    try {
      const response = await axios({
        method: "get",
        url: "http://wdassignment.devfl.com/api/movies",
        headers: { Authorization: `Bearer ${token}` }
      });
      this.setState({
        movies: response.data.data
      });
    } catch (error) {
      error.response.status &&
        this.setState({ errorMsg: error.response.data.message });
    }
  };

  render() {
    const { movies, errorMsg } = this.state;
    return (
      <>
        <TitleWrapper style={{ marginTop: "10%" }}>
          Choose a movie
        </TitleWrapper>
        <Wrapper>
          {movies.map(id => (
            <div key={id}>
              <MovieID id={id} to={`/movie/${id}`} name={id} />
            </div>
          ))}
          {errorMsg}
        </Wrapper>
      </>
    );
  }
}
