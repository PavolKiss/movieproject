import styled from "@emotion/styled";

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 30px;
  align-items: center;
`;

export const TitleWrapper = styled.div`
  font-family: "Raleway", sans-serif;
  font-weight: 200;
  font-style: italic;
  font-size: 50px;
  display: flex;
  justify-content: center;
  margin-top: 15%;
  letter-spacing: 2.5px;
`;

export const Button = styled.button`
  margin-top: 5px;
  outline: none;
  border: 3px solid #e6e6e6;
  background: #e6e6e6;
  font-weight: bold;
  font-family: "Raleway", sans-serif;
  font-size: 14px;
  cursor: pointer;
  padding: 12px;
  width: 120%;
  text-align: center;
  text-decoration: none;
  text-transform: uppercase;
  transition: 0.5s;
  -webkit-transition: 0.5s;

  &:hover {
    background: transparent;
  }
`;

export const ErrorMsg = styled.h5`
  color: #ce2c52;
`;
