import React from "react";
import axios from "axios";
import { Input } from "../Input";
import { Wrapper, Button, TitleWrapper, ErrorMsg } from "./styles";

export default class LoginPage extends React.Component<{ history: any }> {
  state = {
    username: "",
    password: "",
    errorMsg: ""
  };

  onSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault();
    try {
      const response = await axios({
        method: "post",
        url: "http://wdassignment.devfl.com/api/login",
        auth: {
          username: this.state.username,
          password: this.state.password
        },
        headers: {
          Authorization: `Basic ${this.auth}`
        }
      });
      window.sessionStorage.setItem("token", response.data.data.token);
      alert("Now you are logged-in.");
      this.props.history.push("/homepage");
    } catch (error) {
      error.response.status &&
        this.setState({ errorMsg: error.response.data.message });
    }
  };
  handleName = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ username: e.target.value });
  };
  handlePass = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ password: e.target.value });
  };
  auth: any;
  render() {
    const { errorMsg } = this.state;
    return (
      <>
        <TitleWrapper>
          <b>Welcome!</b> Please enter your credentials.
        </TitleWrapper>
        <Wrapper>
          <form onSubmit={this.onSubmit}>
            <div>
              <Input
                type="text"
                onChange={this.handleName}
                placeholder="Enter username"
              />
            </div>
            <div>
              <Input
                type="password"
                onChange={this.handlePass}
                placeholder="Enter password"
              />
            </div>
            <ErrorMsg>{errorMsg}</ErrorMsg>
            <Button>Login</Button>
          </form>
        </Wrapper>
      </>
    );
  }
}
