import styled from "@emotion/styled";

export const LoginInput = styled.input`
  border: 3px solid #d9d9d9;
  outline: none;
  font-family: "Raleway", sans-serif;
  background: white;
  font-size: 16px;
  width: 100%;
  padding: 12px 20px;
  margin-top: 5px;
  transition: 0.5s;
  -webkit-transition: 0.5s;
  font-weight: bold;

  &:focus {
    border: 5px solid #e6e6e6;
  }
`;
