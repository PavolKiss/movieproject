import React, { FC } from "react";
import { LoginInput } from "./styles";

interface IInputProps {
  type: string;
  placeholder: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

export const Input: FC<IInputProps> = ({ type, placeholder, onChange }) => (
  <LoginInput
    type={type}
    required
    minLength={4}
    maxLength={5}
    placeholder={placeholder}
    onChange={onChange}
  />
);
